import ballerina/http;
import wso2/kafka;

//user credentials
@final string username = "user";
@final string password = "user";

// Kafka producer endpoint
endpoint kafka:SimpleProducer kafkaProducer {
    bootstrapServers: "localhost:9092",
    clientID:"server",
    acks:"all",
    noRetries:5
};

// HTTP service endpoint
endpoint http:Listener listener {
    port:9090
};

@http:ServiceConfig {basePath:curl -v -X POST -d \
   '{"Username":"Clinton", "Password":"DSA123", "Filename":"Bamba7", "Size":100.00}' \
   "http://localhost:9090/File/updateFile" -H "Content-Type:application/json"
}
service<http:Service> adminService bind listener {

    @http:ResourceConfig {methods:["POST"], consumes:["application/json"],
        produces:["application/json"]}
    updateFile (endpoint client, http:Request request) {
        http:Response response;
        json reqPayload;
        float newFile;

        //passing our payloads 
        match request.getJsonPayload() {
            // Valid 
            json payload => reqPayload = payload;
            // NOT valid JSON 
            any => {
                response.statusCode = 400;
                response.setJsonPayload({"Message":"Invalid request detected"});
                _ = client -> respond(response);
                done;
            }
        }

          json fileName = reqPayload.File;
        json size = reqPayload.Size;
        json username = reqPayload.Username;
        json password = reqPayload.Password;
      
        if (username == null || password == null || fileName == null || size == null) {
            response.statusCode = 400;
            response.setJsonPayload({"Message":"Bad Request detected"});
            _ = client->respond(response);
            done;
        }

        var result = <float>size.toString();
        match result {
            float value => {
                size = value;
            }
            error err => {
                response.statusCode = 400;
                response.setJsonPayload({"Message":"Invalid file size detected"});
                _ = client->respond(response);
                done;
            }
        }

        // checking user credentials
        if (username.toString() != username || password.toString() != password) {
            response.statusCode = 403;
            response.setJsonPayload({"Message":"Access Denied"});
            _ = client->respond(response);
            done;
        }

        // Construct and serialize the message to be published to the Kafka topic
        json UpdateInfo = {"File":fileName, "UpdateSize":size};
        byte[] serializedMsg = UpdateInfo.toString().toByteArray("UTF-8");

        // Produce the message and publish it to the Kafka topic
        kafkaProducer->send(serializedMsg, "File-Size", partition = 0);
        // Send a success status to the admin request
        response.setJsonPayload({"Status":"Success"});
        _ = client->respond(response);
    }
}