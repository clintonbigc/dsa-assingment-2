

import ballerina/log;
import wso2/kafka;
import ballerina/internal;

// Kafka consumer endpoint
endpoint kafka:SimpleConsumer consumer {
    bootstrapServers: "localhost:9092, 
    // Consumer group ID
    groupId: "client2",
    // Listen from topic UpdateFile
    topics: ["UpdateFile"],
    // Poll every 3 second
    pollingInterval:3000
};

// service to listen to UpdateFile topic
// subscription commit 
service<kafka:Consumer> client1 consumer {
    
    onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        foreach entry in records {
            byte[] serializedMsg = entry.value;
            // Convert the serialized message to string message
            string msg = internal:byteArrayToString(serializedMsg, "UTF-8");
            log:printInfo("New file update received from server");
            // 
            log:printInfo("Topic: " + entry.topic + "; Received Message: " + msg);
            // Acknowledgement
            log:printInfo("Acknowledgement from client2");
        }
    }
}
