// Kafka consumer endpoint
endpoint kafka:SimpleConsumer consumer {
    bootstrapServers: "localhost:9092, 
    // Consumer group ID
    groupId: "distributedFileSysytem",
    // broadcsated topic 'UpdateFile'
    topics: ["UpdateFile"],
    // time to check endpoints
    pollingInterval:3000
};

sub.bal
import ballerina/log;
import wso2/kafka;
import ballerina/internal;

//  consumer endpoint
endpoint kafka:SimpleConsumer consumer {
    bootstrapServers: "localhost:9092",
    // Consumer group ID
    groupId: "distributedFileSysytem",
    // topic 'UpdateFile'
    topics: ["UpdateFile"],
    // check interval
    pollingInterval:3000
};

//service to listen to the topic
service<kafka:Consumer> kafkaService bind consumer {
    
    onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        // processing service one by one
        foreach entry in records {
            byte[] serializedMsg = entry.value;
            
            string msg = internal:byteArrayToString(serializedMsg, "UTF-8");
            log:printInfo("New file updat receivde from endpoint");
            // appendinf new update
            log:printInfo("Topic: " + entry.topic + "; Received Message: " + msg);
           
            log:printInfo("New commits successfully added to atabase");
        }
    }
}